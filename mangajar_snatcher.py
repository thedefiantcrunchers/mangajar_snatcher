#!/usr/bin/env python3

import os
import requests
import argparse
import html
import re
from PIL import Image
from bs4 import BeautifulSoup


def main():
    seriesFlag = args.series

    if seriesFlag:
        process_series(url)
    else:
        process_issue(url)

    print("All Done")
    quit()


def process_series(url):
    r = requests.get(url.rstrip('/') + '/chaptersList')

    soup = BeautifulSoup(r.content, "html.parser")

    issues = soup.find_all('li', {'class': 'chapter-item'})

    for issue in issues:
        process_issue('https://mangajar.com/' + issue.find('a', href=True)['href'])


def process_issue(url):
    r = requests.get(url)

    soup = BeautifulSoup(r.content, "html.parser")

    manga_info = soup.find('h1', {'class': 'h4 mb-2'})

    manga_name = manga_info.find('a', recursive=False).contents[0]
    manga_name = re.sub(r'[\\/*?:"<>|]',"_",manga_name)

    chapter_name = ' '.join(manga_info.contents[2].split())
    chapter_name = re.sub(r'[\\/*?:"<>|]',"_",chapter_name)

    manga_dir = manga_name + os.path.sep + chapter_name

    images = soup.find_all('img', {'class': 'page-image'})

    for image in images:

        file_title = re.sub(r'[\\/*?:"<>|]',"_",image['title'])

        file = fetch_file(image['src'], manga_dir, file_title + ".webp")

        if not os.path.exists(file.replace('.webp', '.jpg')):
            print("Converting " + file)
            im = Image.open(file).convert('RGB')
            im.save(file.replace('.webp', '.jpg'), 'jpeg')
        else:
            print(file + ' already exists')
        # os.remove(file)


def create_dir(directory):
    if not os.path.exists(directory):
        print("Creating " + directory)
        os.makedirs(directory, exist_ok=True)


def fetch_file(url, manga_dir, filename=''):
    manga_dir = base_dir + os.path.sep + manga_dir

    if not os.path.exists(manga_dir + os.path.sep + filename):
        print("Fetching " + manga_dir + os.path.sep + filename)
        manga_file = requests.get(url)

        if manga_file.status_code is 200:
            create_dir(manga_dir)
            open(manga_dir + os.path.sep + filename, 'wb').write(manga_file.content)
        else:
            print('Problem with request - Status code: ' + str(manga_file.status_code))
            return()
    else:
        print(manga_dir + os.path.sep + filename + ' already exists')

    return(manga_dir + os.path.sep + filename)


parser = argparse.ArgumentParser()
parser.add_argument('url', help="URL to parse")
parser.add_argument("--series", action="store_true", help="URL is a series")
parser.add_argument("-o", "--out", dest="dir", default="mangajar", help="Directory to output files. Defaults to 'mangajar'")

args = parser.parse_args()


base_dir = args.dir
url = args.url

main()
